(function(window) {
  "use strict";
  window.app = window.app || {};
  window.app["name"] = "companion-web";
  window.app["version"] = "0.0.0";
  window.app["debug"] = true;
})(window);