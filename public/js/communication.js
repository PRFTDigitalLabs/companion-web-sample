(function(app, faye, q) {
  "use strict";
  app.comm = app.comm || {};

  var publish = function publish(client, channel) {
      return function(type, data) {
        return client.publish(channel, {mode:app.mode, type:type, data:data});
      };
  };

  var close = function close(subscription) {
    return function() {
      subscription.cancel();
    };
  };

  app.comm.connect = function connect(url, channel) {
    var d = q.defer();
    if(url !== null && url !== undefined) {
      app.comm.client = app.comm.client || new faye.Client(url);
      var handlers = {};
      var handler = function handler(type, fn) {
        handlers[type] = fn;
      };

      var client = app.comm.client,
      subscription = client.subscribe(channel, function(message) {
        if(app["debug"]) {
          console.log("message: " + JSON.stringify(message));
        }

        if(message.mode !== app.mode) {
          // Only recieve messages from other types,
          // prevents responding to own message.
          if(handlers && handlers[message.type]) {
            handlers[message.type](message);
          }
        } else {
          if(app["debug"]) {
            console.log("No Handler for:" + message.type);
          }
        }
      });

      subscription.then(function() {
          d.resolve({publish:publish(app.comm.client, channel), close:close(subscription), on:handler});
        }, function(err) {
          d.reject(new Error(err));
        });
    } else {
      d.reject(new Error("url supplied is invalid."));
    }
    return d.promise;
  };

  /*
    var handler = function(){ console.log(""); };
    socket.on("disconnect", handler);
    socket.on("message", handler);
    socket.on("reconnect", handler);
    socket.on("reconnecting", handler);
    socket.on("reconnect_failed", handler);
  */

})(app, Faye, Q);