(function($, app) {
  "use strict";
  console.log("Secondary");
  var token = window.location.pathname.substr(1),
  url = "//" + window.location.host + "/channel";

  var pre = $("#messages");
  var appendResponse = function(message) {
    if(typeof message === "object") message = JSON.stringify(message, null, " ");
    pre.append(message + "\r\n");
  };

  app.comm.connect(url, "/" + token)
  .progress(function(progress){
    console.log(JSON.stringify(progress));
  }).then(function(connection) {
    connection.on("key", function(msg) {
      appendResponse(msg.data.key);
    });
    connection.on("message", function(msg) {
      appendResponse(msg.data.content);
    });
    $(document.body).keypress(function(e) {
      connection.publish("key", { key: String.fromCharCode(e.keyCode) });
    });
    $("#play").on("click", function() {
      connection.publish("cmd", {action: "Play"});
    });
    $("#pause").on("click", function() {
      connection.publish("cmd", {action: "Pause"});
    });
  }).fail(function(e) {
    console.log(e);
  });

})(jQuery, app);