(function($, qr, app) {
  "use strict";
  console.log("Primary");
  $.ajax("/session", { type:"POST" })
    .done(function(data, status) {
      var url = window.location.protocol + data.url;
      $('<img />').attr({ "id": "qrcode",
        "src": qr.generatePNG(url, {modulesize:10}),
        "alt": url
      }).appendTo("#qr");
      var link = $("<a />").attr({"href": url,
        "target":"_blank",
        title: "Connect to Secondary"
      }).html(url)
      .appendTo("#qr");
      var count = 0;
      var pre = $("#messages");
      var appendResponse = function(message) {
        if(typeof message === "object") message = JSON.stringify(message, null, " ");
        pre.append(message + "\r\n");
      };

      if(data) {
        app.comm.connect(data.endpoint, "/" + data.channel)
        .progress(function(progress) {
          console.log(JSON.stringify(progress));
        }).then(function(connection) {
          connection.on("key", function(msg) {
            appendResponse(msg.data.key);
          });

          connection.on("cmd", function(msg) {
            appendResponse(msg.data);
          });

          $(document.body).keypress(function(e) {
            connection.publish("key", {key: String.fromCharCode(e.keyCode) });
          });

          $("#send").on("click", function() {
            connection.publish("message", {content: "Primary Message("+ (++count) +"): Hello from Primary Screen!"});
          });
        }).fail(function(e) {
          console.log(e.message);
        });
      }
    }).fail(function(err) {
      console.log(err);
    });
})(jQuery, QRCode, app);