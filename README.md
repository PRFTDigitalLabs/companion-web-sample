# Companion Web

This project is an example of simple Companion Web functionality.

## Step 1: Install
Download [node.js](http://nodejs.org/ "Node.js"). 

If you are running Windows download node.js [for Windows](http://nodejs.org/dist/v0.10.20/node-v0.10.20-x86.msi). 

##Step 2: Setup
Once you have installed nodejs, open a terminal or *Node.js command prompt* on Windows. Then navigate to the `/companion-web` root directory (the unzipped folder location), and execute the following command:

    npm install

This will install all of the node dependencies listed in the accompanying `packgage.json` file

Then execute the following command to start the node server:

    node ./server/server

##Step 3: Browse App
Open a browser and navigate to [http://localhost:8000](http://localhost:8000). To start companion experience either snap the qrcode, or enter link in companion browser.
