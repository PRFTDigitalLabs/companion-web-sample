module.exports = function BayeuxModule(cfg) {
  "use strict";
  return {
    Bayeux: require("./channel")(cfg)
  };
};