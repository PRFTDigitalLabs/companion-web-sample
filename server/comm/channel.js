module.exports = function ChannelModule(cfg) {
  "use strict";
  var util  = require("util");
  var _bayeux = null;
  var reference = function(bayeux) {
    if(bayeux) {
      _bayeux = bayeux;
      var handlers = new Handlers();
      for(var property in handlers) {
        if(handlers.hasOwnProperty(property)) {
          _bayeux.on(property, handlers[property]);
        }
      }
    } else {
      throw new Error("Faye not initialized.");
    }
  };

  var Handlers = function Handlers() {
    this.handshake = function(clientId){
      util.log("handshake: " + clientId);
    };
    this.disconnect = function(clientId) {
      util.log("disconnect: " + clientId);
    };
    this.subscribe = function(clientId, channel) {
      util.log("subscribe: " + clientId + " channel:" + channel);
    };
    this.unsubscribe = function(clientId, channel) {
      util.log("unsubscribe: " + clientId + " channel:" + channel);
    };
    this.publish = function(clientId, channel, data) {
      util.log("publish: " + clientId + " channel: " + channel + " data: " + util.inspect(data));
    };
  };
  return {
    reference : reference
  };
};