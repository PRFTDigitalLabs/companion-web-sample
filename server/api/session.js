module.exports = function ApiSessionModule(cfg) {
  "use strict";
  var util        = require("util"),
      path        = require("path"),
      controllers = require("../controllers")(cfg);
  
  var port = cfg.server.port,
      endpoint = "//" + cfg.server.host + (port ? (":" + port) : "") + cfg.comm.settings.mount;

  var validate = function(app) {
    app.param(":token", function ValidateToken(req, res, next, token) {
      if(token !== null && token !== undefined) {
        req.token = token;
        next();
      } else {
        next(new Error("IP[" + req.ip + "] requested invalid token: " + token));
      }
    });
  };

  var post = function(app, callback) {
    app.post('/session', function(req, res, next) {
      util.log("/session ip:" + req.ip);
      controllers.session.createSession(req.ip)
        .then(function(info) {
          res.json({ "channel":info.token, "endpoint":endpoint, "url":info.endpoint.url })
            .status(201)
            .end();
          try {
            if(callback && typeof(callback) === 'function') {
              callback(info.token);
            }
          } catch(err) {
            util.log("Silent Error ip[" + req.ip + "]: " + util.inspect(err));
          }
        })
        ["catch"](function(err) {
          util.log("Server Error while responding to '/session': " + util.inspect(err));
          res.status(500)
            .end();
        });
    });
  };

  var get = function(app) {
    var secondary = path.join(app.get("static"), "secondary.html");
    app.get('/:token', function(req, res, next) {
      if(!req.token) {
        next();
        return;
      }

      controllers.session.retrieveSession(req.token)
        .then(function() {
          res.sendfile(secondary);
        })
        ["catch"](function(err) {
          util.log("Server Error while responding to '/session/"+ req.token +"': " + util.inspect(err));
          res.status(500)
            .end();
        });
    });
  };

  return {
    validate : validate,
    post : post,
    get : get
  };
};