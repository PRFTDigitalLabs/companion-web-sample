module.exports = function ApiModule(cfg) {
  "use strict";
  var controllers = require("../controllers")(cfg);
  return {
    Session: require("./session")(cfg, controllers)
  };
};