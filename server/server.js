var path      = require("path"),
    http      = require("http"),
    util      = require("util"),
    express   = require("express"),
    faye      = require('faye'),
    cfg       = require("../config"),
    api       = require("./api")(cfg),
    comm      = require("./comm")(cfg),
    appUtils  = require("./utils");

var app = express(),
    port = cfg.server.port,
    pubdir = path.normalize(path.join(__dirname, "..", cfg.server["public"])),
    server = http.createServer(app),
    bayeux = new faye.NodeAdapter(cfg.comm["settings"]);

var StartApplication = function() {
  bayeux.attach(server);
  server.listen(port);
  util.log("Started Server on port: " + port);
};

// Initialize Bayeux Endpoint
comm.Bayeux.reference(bayeux);

// Configure Express Web Server
app.configure(function ExpressConfiguration() {
  // Add Static Folder Dir to App Values
  app.set('static', pubdir);
  // Init Middleware
  app.use(express.static(app.get('static')));
  app.use(express.limit("1mb"));
  app.use(express.json());
  app.use(express.urlencoded());
});

// Initialize API Endpoints
api.Session.validate(app);
api.Session.get(app);
api.Session.post(app);

// Now Start Application
StartApplication();
