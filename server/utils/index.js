module.exports = (function UtilsModule() {
  "use strict";
  var _     = require("underscore");
  return {
    token: function(length) {
      if(!_.isFinite(length) || length <= 0 || length > 16) throw Error("Token Length is invalid");
      return Math.random().toString(36).substr(2, length + 2);
    }
  };
})();