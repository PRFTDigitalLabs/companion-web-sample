module.exports = function ControllerModule(cfg){
  "use strict";
  return {
    session: require("./session")(cfg)
  };
};