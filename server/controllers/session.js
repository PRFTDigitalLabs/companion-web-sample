module.exports = function ConnectionModule(cfg) {
  "use strict";
  var q     = require("q"),
      utils = require("../utils");

  var port = cfg.server.port,
      host = "" + cfg.server.host + (port ? (":" + port) : "");
  
  // For example purposes no backing database is 
  // used to store session information. This would be necessary
  // for deployed solutions with more than one Node instance
  var sessions = {};

  var createSession = function createSession (ip) {
    var d = q.defer();
    var token = utils.token(4),
        session = {
          token: token,
          primary: ip,
          secondary: [],
          timestamp: new Date(),
          endpoint: {
            url: "//" + host + "/" + token,
            channel: "//" + host + "/channel/" + token
          }
        };
      sessions[token] = ip;
    d.resolve(session);
    return d.promise;
  };
  
  var retrieveSession = function retrieveSession(token) {
    var d = q.defer();
    if(token in sessions) {
      d.resolve({token:token});
    } else {
      d.reject(new Error("Session not found."));
    }
    return d.promise;
  };

  return {
    createSession: createSession,
    retrieveSession : retrieveSession
  };
};