module.exports = (function ConfigurationModule(){
  
  var fs     = require("fs"),
      os     = require("os"),
      path   = require("path"),
      util   = require("util"),
      extend = require("node.extend"),
      global = require("./config.global");

  if(!process.env["NODE_ENV"]) {
    // Should Throw under production/non-example code use case
    // throw new Error('Configuration failed: No Environment Specified.');
    util.error('Configuration Issue: No Environment Specified.');
    util.error('Setting Configuration to development.');
    process.env["NODE_ENV"] = "development";
  }

  // Method to retrieve External ip address
  var networks = os.networkInterfaces();
  var ip = function(version, interfaces) {
    version = version || "IPv4";
    interfaces = interfaces || ["Ethernet", "en0", "en1", "en2", "en3", "en4", "en5", "Wi-Fi"];

    for(var i = 0; i < interfaces.length; ++i) {
      if(interfaces[i] in networks) {
        detail = networks[interfaces[i]].filter(function(el){ return !el.internal && el.family === version; });
        if(detail && detail.length > 0) {
          // Brackets are necessary for use in browser
          ip = detail[0].family === "IPv6" ? ("[" + detail[0].address + "]") : detail[0].address;
          return {address:ip, version:version};
        }
      }
    }
    return {address:null, version:version};
  };

  // Attempt to read environment specific configuration
  var local = path.join(__dirname, "./config." + process.env["NODE_ENV"] + ".json");
  local = fs.existsSync(local) ? require(local) : {};

  // Read External Environment Settings
  var env = {
    // Server Settings
    server: {
      host: process.env["HOST"],
      port: process.env["PORT"]
    }
  };

  // Flatten & Freeze configuration, local overrides global, and environ overrrides locals
  var config = extend(true, {}, global, local, env);

  // Attempt to find external ip for same network connections (only in development mode)
  if(process.env["NODE_ENV"] === "development" && config.server.host === "localhost") {
    host = ip("IPv4");
    if(host) {
      if(host.address !== null && host.address !== undefined) {
        util.log("Configuration: Replacing server.host value \"" + 
          config.server.host + "\" with external " + host.version + " address \"" +host.address+ "\".");
        config.server.host = host.address;
      } else {
        util.error("Configuration Issue: Could not replace server.host value \"" + 
          config.server.host + "\" with external " + host.version + " address.");
      }
    }
  }

  config = Object.freeze(config);
  util.log("Configuration: " + util.inspect(config));
  return config;

})();